import requests

def replace_spaces(put_url):
    new_str = ""
    for i in put_url:
        if i == " ":
            i = "+"
        new_str += i
    return new_str

title = input("Title? : ")
#genre = input("Genre? : ")
year = input("Year? : ")

#not to show my api_key, i read my key from a file
api_key_file = open("api_key.txt", "r")
api_key = api_key_file.readline()
api_key_file.close()

url = "http://www.omdbapi.com/?apikey={}&t={}&y={}".format(api_key,title,year) #use your own api key in the url

proper_url = replace_spaces(url)

response = requests.get(proper_url)
response_json = response.json()

if response.status_code == 200:
    print("status code: {}".format(response.status_code))
    print(response_json)
else:
    print("An error occured!\nstatus code: {}".format(response.status_code))

    



